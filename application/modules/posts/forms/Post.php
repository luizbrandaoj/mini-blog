<?php

class Posts_Form_Post extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $this->setName('Post')
	     ->setOptions(array('class'=>'form-horizontal'));

        $id = new Zend_Form_Element_Hidden('_id');

        $author = new Zend_Form_Element_Text('author');
        $author->setLabel('Autor')
        	   ->setRequired(true)
        	   ->addFilter('StripTags')
        	   ->addFilter('StringTrim')
        	   ->addValidator('NotEmpty');

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Title')
        	  ->setRequired(true)
        	  ->addFilter('StripTags')
        	  ->addFilter('StringTrim')
        	  ->addValidator('NotEmpty');

        $body = new Zend_Form_Element_Text('body');
        $body->setLabel('Texto')
        	  ->setRequired(true)
        	  ->addFilter('StripTags')
        	  ->addFilter('StringTrim')
        	  ->addValidator('NotEmpty');	  

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id','submitbutton')
		->setLabel('Adicionar')
		->setOptions(array('class'=>'btn btn-success'));

        $this->addElements(array($id,$author,$title,$body,$submit));
    }
}
