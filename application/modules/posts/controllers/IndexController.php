<?php

class Posts_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
	// action body
        $post = new Posts_Model_Post();
        $posts = $post->viewAll();
        $this->view->posts = $posts;
    }

    public function addAction()
    {
        // action body
	$form = new Posts_Form_Post();
        $form->submit->setLabel('Adicionar');
        $this->view->form = $form;

        if($this->getRequest()->isPost()){
                $formData = $this->getRequest()->getPost();
                if($form->isValid($formData)){
                        $post = new Application_Model_Post();
                        $post->author = $form->getValue('author');
                        $post->title = $form->getValue('title');
                        $post->body = $form->getValue('body');
                        $posts = new Posts_Model_Post();
                        if($posts->createAction($post)){
                                $this->_helper->redirector('index','index','posts');
                        }
                }else{
                        $form->populate($formData);
                }
     	}
    }

    public function listAction()
    {
	$post = new Posts_Model_Post();
	$posts = $post->viewAll();
	$this->view->post = $posts;
    }

    public function updateAction()
    {
	$form = new Posts_Form_Post();
	$form->submit->setLabel('Atualizar');
	$this->view->form = $form;

	if($this->getRequest()->isPost()){
                $formData = $this->getRequest()->getPost();
                if($form->isValid($formData)){
                        $post = new Application_Model_Post();
			$post->id = $form->getValue('_id');
                        $post->author = $form->getValue('author');
                        $post->title = $form->getValue('title');
                        $post->body = $form->getValue('body');
                        $posts = new Posts_Model_Post();
                        if($posts->updatePost($post)){
                                $this->_helper->redirector('index','index','posts');
                        }
                }else{
                        $form->populate($formData);
                }
        } else {
		$id = $this->_getParam('id',0);
		if($id > 0){
			$post = new Posts_Model_Post();
			$form->populate($post->find($id));
		}
	}	
    }

    public function removeAction()
    {
        // action body
	$id = $this->_getParam('id',0);
	$post = new Posts_Model_Post();
	$post->deletePost($id);
	$this->_helper->redirector('index','index','posts');
    }

    public function viewAction()
    {
        // action body
	$id = $this->_getParam('id', 0);
	$posts = new Posts_Model_Post();
	$post = $posts->find($id);
	$this->view->post = $post;
    }
}
