<?php

class Posts_Model_Post
{
	public function getConnection(){
		$m = new Mongo();

		$db = $m->refac;

		$collection = $db->posts;
		return $collection;
	}

	public function createAction($post){
		$blogPost = new Application_Model_Post();
		$blogPost->author = $post->author;
		$blogPost->title = $post->title;
		$blogPost->body = $post->body;
		$posts = array(
			'author'=>$blogPost->author,
			'title'=>$blogPost->title,
			'body'=>$blogPost->body
		);
		$collection = $this->getConnection();
		$collection->save($posts);
		return true;
	}

	public function updatePost($post){
		try{
			$collection = $this->getConnection();
			$collection->update(
				array('_id' => new MongoId($post->id)),
                        	array(
					'$set'=>array(
						'author' => $post->author,
                        			'title' => $post->title,
                        			'body' => $post->body,
					)
				)
			);
			return true;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public function deletePost($id) {
		$collection = $this->getConnection();
		$criteria = array('_id'=> new MongoId($id));
		$collection->remove($criteria, array("justOne" => true) );
	}

	public function viewAll() {
		$collection = $this->getConnection();
		$posts = $collection->find();
		return $posts;
	}
	public function find($id){
		$collection = $this->getConnection();
		$post = $collection->findOne(array('_id' => new MongoId($id)));
		return $post;
	}
}
